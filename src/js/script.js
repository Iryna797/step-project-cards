// import { ..... } from './modules/.....js';
import sign_in from "./modules/sign_in.js";
import show_modal from "./modules/show_modal.js";
import modal__cancel from "./modules/modal__cancel.js";
import sign_in_compare from "./modules/sign_in_compare.js";
import choose_doctor from "./modules/choose_doctor.js";
import status_filter from "./modules/status_filter.js";
import urgency_filter from "./modules/urgency_filter.js";
import search_text from "./modules/search_text.js";

//Logged user:
//==== let user_login = "admin";
//==== let user_pass = "12345zxc";

//==== localStorage.setItem("user_login", user_login);
//==== localStorage.setItem("user_pass", user_pass);

//-------------------------------------------------------------------------------------
// let backend_cards = [];
let modal_array = [0];
//-------------------------------------------------------------------------------------

let sign_in_btn = document.querySelector(".login-btn");
sign_in_btn.addEventListener("click", () => {
  sign_in("sign_in_form");
});
// Ф-ция назначена на кнопку Go! в форме ввода логина-пароля.
// Если введен правильный логин-пароль, окно ичезает, кнопка "Вход" убирется,
// показывается кнопка "Создать визит"
let sign_in_form_btn = document.querySelector(".sign_in_form_btn");
sign_in_form_btn.addEventListener("click", () => {
  sign_in_compare();
});
//--------------------------------------------------------------------------------------
let visit_btn = document.querySelector(".create-btn");
visit_btn.addEventListener("click", () => {
  show_modal(modal_array);
});
//--------------------------------------------------------------------------------------
// Ф-ция назначена на кнопку "Закрыть" модального окна. Закрывает окно,
// чистит все поля ввода.
let modal__no = document.querySelector(".modal__no");
modal__no.addEventListener("click", modal__cancel);
//--------------------------------------------------------------------------------------
// Выбор доктора в модальном окне
let modal__doctor = document.querySelector(".modal__doctor");
modal__doctor.addEventListener("change", choose_doctor);
//--------------------------------------------------------------------------------------
// Функция подтверждения информации, введенной в модальное окно.
// Назначена на кнопку "Создать".

let modal__yes = document.querySelector(".modal__yes");
modal__yes.onclick = () => {
  modal_accept(modal_array);
};
//--------------------------------------------------------------------------------------
// Фильтр по статусу визита.
// Фильтруем так. Находим все элементы с классом ".visit .visit__status" (сюда записан статус).
// Если у элемена текстКонтент === "Виконано" --> родителя (карту) показываем, остальным присваиваем класс hidden.
// Вешаем эту функцию на событие change соотв. селекта.

let search__completed = document.querySelector(".search__completed");
search__completed.addEventListener("change", status_filter);
//--------------------------------------------------------------------------------------
// Фильтр по статусу визита.
// Фильтруем так. Находим все элементы с классом ".visit .visit__status" (сюда записан статус).
// Если у элемена текстКонтент === "Виконано" --> родителя (карту) показываем, остальным присваиваем класс hidden.
// Вешаем эту функцию на событие change соотв. селекта.

let search__urgency = document.querySelector(".search__urgency");
search__urgency.addEventListener("change", urgency_filter);
//--------------------------------------------------------------------------------------
// Поиск по тексту в цели визита и кратком описании
// Срабатывает по вводу символов в соотв. инпут.
let search__title = document.querySelector(".search__title");
search__title.addEventListener("input", search_text);
