function sign_in(sign_in_form_class) {
    let sign_in_form = document.querySelector(`.${sign_in_form_class}`);
    sign_in_form.classList.remove("hidden");

    // Скрываем форму при клике мимо самой формы и кнопки "Вход"
    let sign_in_btn = document.querySelector(".login-btn");
    document.onclick = (e) => {
        let click = e.composedPath().includes(sign_in_form);
        let click2 = e.composedPath().includes(sign_in_btn);

        // console.log(click, click2);
        if (click === false && click2 === false) { 
            sign_in_form.classList.add("hidden");
        }
    }
}

export default sign_in;