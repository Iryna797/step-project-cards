// Удаление карточки из базы данных сервера. И с экрана..
async function del_card(id) {
  //=== let token = "885f873a-f332-4e4a-98f5-fd0c310126e2";
  let token = localStorage.getItem("token");
  // console.log("del_card token", token);
  let request = await fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
      method: 'DELETE',
      headers: {
        'Authorization': `Bearer ${token}`
      },
    });
  // console.log(`DEL id=${id}`, request);

  let backend_cards = JSON.parse(localStorage.getItem("backend_cards")); ///STORAGE
  
    // Если сервер удалил, убираем с экрана карту.
  if(request.ok === true && request.status === 200){
      let card_to_del = document.querySelector(`.id${id}`);
      // console.log(card_to_del);
      card_to_del.remove();
    
      // Так же удаляем эту карту из массива backend_cards.
    
      let temp_arr = [];
      for(let i = 0; i < backend_cards.length; i++){
        if(backend_cards[i].id === id) {
          temp_arr = backend_cards.splice(i, 1);
          // console.log("eeeeeee");
        }
      }   
    localStorage.setItem("backend_cards", JSON.stringify(backend_cards)); /// STORAGE
    localStorage.setItem("test", "test2");
      // console.log("backend_cards[] after del:", backend_cards);
  }
   // Если удалили все карты, показываем надпись "No items have been added".
  if (!document.querySelector(".visit")) { 
    document.querySelector(".missing__elements").classList.remove("hidden");
  }
}

export default del_card;