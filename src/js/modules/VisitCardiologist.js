import Visit from "./Visit.js";

class VisitCardiologist extends Visit {
    constructor(id, name, goal, description, priority, pressure, weight, heart, age, status){
        super(id, name, goal, description, priority, status);
        this.pressure = pressure;
        this.weight = weight;
        this.heart = heart;
        this.age = age;
    }
    render(){
        let missing__elements = document.querySelector(".missing__elements");
        missing__elements.classList.add("hidden");
        let visits__block = document.querySelector(".visits__block");
        let visit = document.createElement("div");
        visit.classList.add(`id${this.id}`);
        visit.classList.add("visit");
        visit.classList.add("shadow");
      visit.innerHTML = `
      <div class="visit__btn-control">
          <button class="btn-more" alt="more info">
            <i class="fa-thin fa-memo-circle-info"></i>
          </button>
          <button class="btn-edit">
            <i class="fa-light fa-pen-to-square"></i>
          </button>
          <button class="btn-delete">
            <i class="fa-thin fa-trash-can-xmark"></i>
          </button>
        </div>
        <div class="visit__fullname">${this.name}</div>
        <div class="visit__doctor"><b>Кардіолог</b></div>
        <div class="visit__status">${this.status}</div>
        <div class="visit__more-info hidden">
          <div class="visit__purpose">
            Мета візиту:
            <span class="purpose__value">${this.goal}</span>
          </div>
          <div class="visit__brief">
            Короткий опис візиту:
            <span class="brief__value">${this.description}</span>
          </div>
          <div class="visit__urgency">
            Терміновість візиту:
            <span class="urgency__value">${this.priority}</span>
          </div>
          <div class="visit__pressure">
            Звичайний тиск:
            <span class="pressure__value">${this.pressure}</span>
          </div>
          <div class="visit__mass-index">
           Вага:
            <span class="mass-index__value">${this.weight}</span>
          </div>
          <div class="visit__disease">
            Перенесені захворювання серцево-судинної системи:
            <span class="disease__value">${this.heart}</span>
          </div>
          <div class="visit__age">
            Вік:
            <span class="age__value">${this.age}</span>
          </div>
          <div class="visit__last-date-visit hidden">
            Дата останнього візиту:
            <span class="last-date-visit__value"></span>
          </div>
        </div> 
        `;

        visits__block.append(visit);
    }
    
}

export default VisitCardiologist;