function search_text(e) {
  // console.log(e.target.value);

  //Сначала скрываем все карты, потом откроем те, которые содержат искомый текст.
  let cards = document.querySelectorAll(".visit");
  for (let card of cards) { 
    card.classList.add("hidden");
  }

  let all_goal = document.querySelectorAll(".visit .visit__more-info .visit__purpose .purpose__value");
  let all_description = document.querySelectorAll(".visit .visit__more-info .visit__brief .brief__value");


  for (let elem of all_goal) {    
    if (elem.textContent.toLowerCase().includes(e.target.value.toLowerCase())) { 
      elem.parentElement.parentElement.parentElement.classList.remove("hidden");
      // console.log("goal:", elem.textContent);
    }
  }
  for (let elem of all_description) {    
    if (elem.textContent.toLowerCase().includes(e.target.value.toLowerCase())) { 
      elem.parentElement.parentElement.parentElement.classList.remove("hidden");
      // console.log("description:", elem.textContent);      
    }
  }
}

export default search_text;