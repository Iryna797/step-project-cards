class Visit {
    constructor(id, name, goal, description, priority, status) { 
      this.id = id;
      this.name = name;
      this.goal = goal;
      this.description = description;
      this.priority = priority;
      this.status = status;
    }
}

export default Visit;