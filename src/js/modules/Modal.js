class Modal { 
    constructor(id, name, doctor, goal, description, priority, pressure, weight, heart, age, last_visit, status) { 
      this.id = id;
      this.name = name;
      this.doctor = doctor;
      this.goal = goal;
      this.description = description;
      this.priority = priority;
      this.pressure = pressure;
      this.weight = weight;
      this.heart = heart;
      this.age = age;
      this.last_visit = last_visit;
      this.status = status;
    }
}

export default Modal;