function choose_doctor(event) {
    let modal__pressure = document.querySelector(".modal__pressure");
    let modal__pressure_titles = document.querySelector(".modal__pressure_titles");
    let modal__weight = document.querySelector(".modal__weight");
    let modal__weight_titles = document.querySelector(".modal__weight_titles");
    let modal__heart = document.querySelector(".modal__heart");
    let modal__heart_titles = document.querySelector(".modal__heart_titles");
    let modal__age = document.querySelector(".modal__age");
    let modal__age_titles = document.querySelector(".modal__age_titles");
    let modal__last_visit = document.querySelector(".modal__last_visit");
    let modal__last_visit_titles = document.querySelector(".modal__last_visit_titles");

    // console.log(event.target.value);
    if (event.target.value === "cardio") { 
        modal__pressure.classList.remove("hidden");
        modal__pressure_titles.classList.remove("hidden");
        modal__weight.classList.remove("hidden");
        modal__weight_titles.classList.remove("hidden");
        modal__heart.classList.remove("hidden");
        modal__heart_titles.classList.remove("hidden");
        modal__age.classList.remove("hidden");
        modal__age_titles.classList.remove("hidden");
        modal__last_visit.classList.add("hidden");
        modal__last_visit_titles.classList.add("hidden");
    }
    if (event.target.value === "dentist") { 
        modal__pressure.classList.add("hidden");
        modal__pressure_titles.classList.add("hidden");
        modal__weight.classList.add("hidden");
        modal__weight_titles.classList.add("hidden");
        modal__heart.classList.add("hidden");
        modal__heart_titles.classList.add("hidden");
        modal__age.classList.add("hidden");
        modal__age_titles.classList.add("hidden");
        modal__last_visit.classList.remove("hidden");
        modal__last_visit_titles.classList.remove("hidden");
    }
    if (event.target.value === "thera") { 
        modal__pressure.classList.add("hidden");
        modal__pressure_titles.classList.add("hidden");
        modal__weight.classList.add("hidden");
        modal__weight_titles.classList.add("hidden");
        modal__heart.classList.add("hidden");
        modal__heart_titles.classList.add("hidden");
        modal__age.classList.remove("hidden");
        modal__age_titles.classList.remove("hidden");
        modal__last_visit.classList.add("hidden");
        modal__last_visit_titles.classList.add("hidden");
    }
}

export default choose_doctor;