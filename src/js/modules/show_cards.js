import VisitDentist from "./VisitDentist.js";
import VisitCardiologist from "./VisitCardiologist.js"
import VisitTherapist from "./VisitTherapist.js";
import del_card from "./del_card.js";
import edit_card from "./edit_card.js";

async function show_cards(token) {
            let request = await fetch("https://ajax.test-danit.com/api/v2/cards", {
                method: 'GET',
                headers: {
                  'Authorization': `Bearer ${token}`
                },
              });
            let response = await request.json();
  
            let backend_cards = response;
  localStorage.setItem("backend_cards", JSON.stringify(backend_cards)); /// STORAGE
  localStorage.setItem("test", "test"); /// STORAGE
  
            // console.log(response, backend_cards);

            // Создаем объект соотв. доктора (по свойству из ответа сервера) и "рисуем" карту
            // на экране
            
            for(let obj of response){
                if(obj.doctor === "dentist"){
                    //(id, name, goal, description, priority, last_visit)
                    let visit_dentist = new VisitDentist(
                      obj.id,
                      obj.name,
                      obj.goal,
                      obj.description,
                      obj.priority,
                      obj.last_visit,
                      obj.status
                  );
                 
                  visit_dentist.render(obj.id);
                  // Добавляем события на кнопки delete, more карточек
                  // Кнопка btn-delete
                  let btn_delete = document.querySelector(`.id${obj.id} .visit__btn-control .btn-delete`);
                  btn_delete.addEventListener("click", () => { del_card(obj.id, backend_cards) });
                  // Кнопка btn-more
                  let btn_more = document.querySelector(`.id${obj.id} .visit__btn-control .btn-more`);
                  btn_more.onclick = () => { 
                    document.querySelector(`.id${obj.id} .visit__more-info`).classList.toggle("hidden");
                  }
                  // Кнопка btn-edit
                  let btn_edit = document.querySelector(`.id${obj.id} .visit__btn-control .btn-edit`);
                  btn_edit.addEventListener("click", () => { edit_card(obj.id, backend_cards) });
                }
                if(obj.doctor === "cardio"){
                    //(id, name, goal, description, priority, pressure, weight, heart, age)
                    let visit_cardio = new VisitCardiologist(
                      obj.id,
                      obj.name,
                      obj.goal,
                      obj.description,
                      obj.priority,
                      obj.pressure,
                      obj.weight,
                      obj.heart,
                      obj.age,
                      obj.status
                    );
                  visit_cardio.render(obj.id);
                  // Добавляем события на кнопки delete, more карточек
                  // Кнопка btn-delete
                  let btn_delete = document.querySelector(`.id${obj.id} .visit__btn-control .btn-delete`);
                  btn_delete.addEventListener("click", () => { del_card(obj.id, backend_cards) });
                  // Кнопка btn-more
                  let btn_more = document.querySelector(`.id${obj.id} .visit__btn-control .btn-more`);
                  btn_more.onclick = () => { 
                    document.querySelector(`.id${obj.id} .visit__more-info`).classList.toggle("hidden");
                  }
                  // Кнопка btn-edit
                  let btn_edit = document.querySelector(`.id${obj.id} .visit__btn-control .btn-edit`);
                  btn_edit.addEventListener("click", () => { edit_card(obj.id, backend_cards) });
                }
                if(obj.doctor === "thera"){
                    //(id, name, goal, description, priority, age)
                    let visit_thera = new VisitTherapist(
                      obj.id,
                      obj.name,
                      obj.goal,
                      obj.description,
                      obj.priority,
                      obj.age,
                      obj.status
                    );
                  visit_thera.render(obj.id);
                  // Добавляем события на кнопки delete, more карточек
                  // Кнопка btn-delete
                  let btn_delete = document.querySelector(`.id${obj.id} .visit__btn-control .btn-delete`);
                  btn_delete.addEventListener("click", () => { del_card(obj.id, backend_cards) });
                  // Кнопка btn-more
                  let btn_more = document.querySelector(`.id${obj.id} .visit__btn-control .btn-more`);
                  btn_more.onclick = () => { 
                    document.querySelector(`.id${obj.id} .visit__more-info`).classList.toggle("hidden");
                  }
                  // Кнопка btn-edit
                  let btn_edit = document.querySelector(`.id${obj.id} .visit__btn-control .btn-edit`);
                  btn_edit.addEventListener("click", () => { edit_card(obj.id, backend_cards) });
                }
            }
        }

export default show_cards;