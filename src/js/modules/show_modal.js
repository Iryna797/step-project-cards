import modal_accept from "./modal_accept.js";

// Ф-ция показывает модальное окно, скрывает - при клике мимо самой формы и кнопки "Создать визит".
function show_modal(modal_array, backend_cards) {
    let modal = document.querySelector(".modal");
    modal.classList.remove("hidden");
  let modal__yes = document.querySelector(".modal__yes");
  
  // console.log("backend_cards --- show_modal", backend_cards);
  
    modal__yes.onclick = () => { modal_accept(modal_array, backend_cards) };
  
    modal__yes.textContent = "Створити";

    // Скрываем форму модального окна при клике мимо самой формы и кнопки "Создать визит"
    let visit_btn = document.querySelector(".create-btn");
    let modal_goal = document.querySelector(".modal_goal");
    let modal__description = document.querySelector(".modal__description");
    let modal__name = document.querySelector(".modal__name");
    let modal__pressure = document.querySelector(".modal__pressure");
    let modal__weight = document.querySelector(".modal__weight");
    let modal__heart = document.querySelector(".modal__heart");
    let modal__age = document.querySelector(".modal__age");
    let modal__last_visit = document.querySelector(".modal__last_visit");
    let btn_edit = document.querySelectorAll(".btn-edit");
    

    document.onclick = (e) => {
        let click = e.composedPath().includes(modal);
        let click2 = e.composedPath().includes(visit_btn);
        // Так как кнопок ЭДИТ может быть много (по кол-ву карточек),
        // то ищем их все. Нужно скрыть модальное окно при нажатии мимо 
        // этих кнопок _и_ мимо модального окна _и_ мимо кнопки "создать визит"..
        function click_3() {
          let res = false;
          for(let i = 0; i < btn_edit.length; i++){
            res = res || e.composedPath().includes(btn_edit[i]);
          }
          // console.log("res = ",res);
          return res;
        }
        let click3 = click_3();
        /////////

        if(btn_edit){ 
          if (click === false && click2 === false && click3 === false) { 
            modal.classList.add("hidden");
            modal_goal.value = "";
            modal__description.value = "";
            modal__name.value = "";
            modal__pressure.value = "";
            modal__weight.value = "";
            modal__heart.value = "";
            modal__age.value = "";
            modal__last_visit.value = "";
          }
        }
        else {
          // console.log(click, click2);
          if (click === false && click2 === false) { 
              modal.classList.add("hidden");
              modal_goal.value = "";
              modal__description.value = "";
              modal__name.value = "";
              modal__pressure.value = "";
              modal__weight.value = "";
              modal__heart.value = "";
              modal__age.value = "";
              modal__last_visit.value = "";
          }
        }
    }
}

export default show_modal;