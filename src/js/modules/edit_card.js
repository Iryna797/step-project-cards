import modal__cancel from "./modal__cancel.js";
import Modal from "./Modal.js";

function edit_card(id) {
  // Показываем модальное окно, берем информацию из карты, которую редактируем
  function show_current_modal() {
    let modal = document.querySelector(".modal");
    let modal__yes = document.querySelector(".modal__yes");
    modal__yes.textContent = "Редагувати";
    
    // Скрываем форму модального окна при клике мимо самой формы и кнопок ЕДИТ всех карточек
    let modal__status = document.querySelector(".modal__status");
    let modal__doctor = document.querySelector(".modal__doctor");
    let modal_goal = document.querySelector(".modal_goal");
    let modal__description = document.querySelector(".modal__description");
    let modal__name = document.querySelector(".modal__name");
    let modal__priority = document.querySelector(".modal__priority");
    let modal__pressure = document.querySelector(".modal__pressure");
    let modal__pressure_titles = document.querySelector(".modal__pressure_titles");
    let modal__weight = document.querySelector(".modal__weight");
    let modal__weight_titles = document.querySelector(".modal__weight_titles");
    let modal__heart = document.querySelector(".modal__heart");
    let modal__heart_titles = document.querySelector(".modal__heart_titles");
    let modal__age = document.querySelector(".modal__age");
    let modal__age_titles = document.querySelector(".modal__age_titles");
    let modal__last_visit = document.querySelector(".modal__last_visit");
    let modal__last_visit_titles = document.querySelector(".modal__last_visit_titles");
    let btn_edit = document.querySelectorAll(".btn-edit");
    

    document.onclick = (e) => {
        let click = e.composedPath().includes(modal);
        // let click2 = e.composedPath().includes(visit_btn);
        // Так как кнопок ЭДИТ может быть много (по кол-ву карточек),
        // то ищем их все. Нужно скрыть модальное окно при нажатии мимо 
        // этих кнопок _и_ мимо модального окна _и_ мимо кнопки "создать визит"..
        function click_3() {
          let res = false;
          for(let i = 0; i < btn_edit.length; i++){
            res = res || e.composedPath().includes(btn_edit[i]);
          }
          // console.log("res = ",res);
          return res;
        }
        let click3 = click_3();

          if (click === false && click3 === false) { 
            modal.classList.add("hidden");
            modal_goal.value = "";
            modal__description.value = "";
            modal__name.value = "";
            modal__pressure.value = "";
            modal__weight.value = "";
            modal__heart.value = "";
            modal__age.value = "";
            modal__last_visit.value = "";
          }
    }

    // Ищем индекс элемент массива backend_cards[], который содержит ID
    // редактируемой карты
    
  let backend_cards = JSON.parse(localStorage.getItem("backend_cards")); ///STORAGE

    let card_index = 0;
    for(let i = 0; i < backend_cards.length; i++){
      if(backend_cards[i].id === id) {
        card_index = i;
        // console.log("edited card_index", card_index);
        break;        
      }
    }  
    // localStorage.setItem("backend_cards", JSON.stringify(backend_cards)); /// STORAGE
    // console.log("backend_cards[]", backend_cards);
    // console.log("card_index", card_index);

    // Заполняем поля открывающегося модального окна информацией из массива backend_cards[]
    // Выбор доктора в модальном окне
    if (backend_cards[card_index].doctor === "cardio") {
        modal__status.value = backend_cards[card_index].status;
        modal__doctor.value = "cardio";
        modal_goal.value = backend_cards[card_index].goal;
        modal__description.value = backend_cards[card_index].description;
        modal__name.value = backend_cards[card_index].name;
        modal__pressure.classList.remove("hidden");
        modal__pressure_titles.classList.remove("hidden");
        modal__pressure.value = backend_cards[card_index].pressure;
        modal__weight.classList.remove("hidden");
        modal__weight_titles.classList.remove("hidden");
        modal__weight.value = backend_cards[card_index].weight;
        modal__heart.classList.remove("hidden");
        modal__heart_titles.classList.remove("hidden");
        modal__heart.value = backend_cards[card_index].heart;
        modal__age.classList.remove("hidden");
        modal__age_titles.classList.remove("hidden");
        modal__age.value = backend_cards[card_index].age;
        modal__last_visit.classList.add("hidden");
        modal__last_visit_titles.classList.add("hidden");
        modal__last_visit.value = backend_cards[card_index].last_visit;
    }
    if (backend_cards[card_index].doctor === "dentist") {
        modal__status.value = backend_cards[card_index].status;
        modal__doctor.value = "dentist";
        modal_goal.value = backend_cards[card_index].goal;
        modal__description.value = backend_cards[card_index].description;
        modal__name.value = backend_cards[card_index].name;
        modal__pressure.classList.add("hidden");
        modal__pressure_titles.classList.add("hidden");
        modal__pressure.value = backend_cards[card_index].pressure;
        modal__weight.classList.add("hidden");
        modal__weight_titles.classList.add("hidden");
        modal__weight.value = backend_cards[card_index].weight;
        modal__heart.classList.add("hidden");
        modal__heart_titles.classList.add("hidden");
        modal__heart.value = backend_cards[card_index].heart;
        modal__age.classList.add("hidden");
        modal__age_titles.classList.add("hidden");
        modal__age.value = backend_cards[card_index].age;
        modal__last_visit.classList.remove("hidden");
        modal__last_visit_titles.classList.remove("hidden");
        modal__last_visit.value = backend_cards[card_index].last_visit;
    }
    if (backend_cards[card_index].doctor === "thera") {
        modal__status.value = backend_cards[card_index].status;
        modal__doctor.value = "thera";
        modal_goal.value = backend_cards[card_index].goal;
        modal__description.value = backend_cards[card_index].description;
        modal__name.value = backend_cards[card_index].name;
        modal__pressure.classList.add("hidden");
        modal__pressure_titles.classList.add("hidden");
        modal__pressure.value = backend_cards[card_index].pressure;
        modal__weight.classList.add("hidden");
        modal__weight_titles.classList.add("hidden");
        modal__weight.value = backend_cards[card_index].weight;
        modal__heart.classList.add("hidden");
        modal__heart_titles.classList.add("hidden");
        modal__heart.value = backend_cards[card_index].heart;
        modal__age.classList.remove("hidden");
        modal__age_titles.classList.remove("hidden");
        modal__age.value = backend_cards[card_index].age;
        modal__last_visit.classList.add("hidden");
        modal__last_visit_titles.classList.add("hidden");
        modal__last_visit.value = backend_cards[card_index].last_visit;
    }

    // Показываем заполенное модальное окно
    modal.classList.remove("hidden");

    ////===========================
    // Функция подтверждения информации, введенной в модальное окно.
// Назначена на кнопку "Создать".

// modal__yes.removeEventListener("click", modal_accept);
// modal__yes.addEventListener("click", modal_edit);
modal__yes.onclick = modal_edit;

// Массив, содержащий "модальные окна". Чтоб индексы элементов совпадали с ID, 
// нулевой элемент будет содержать "0".
// let modal_array = [0];

    function modal_edit() {
   
  // Проверка полей на "пустоту". Если пусто - показываем алерт "введи инфо!"
let pressure_arr = modal__pressure.value.split("/");
  
  if (modal_goal.value === "" || modal__description.value === "" || modal__name.value === "") {
    alert("Будь-ласка, заповніть дані");
  }
  //=-=-=-=-=-=-=-==-=-=-=-=-==-=-=-=-=-=-=-
  else if(modal__doctor.value === "cardio" && (modal__pressure.value.indexOf("/") === -1 ||
          isNaN(pressure_arr[0].trim()) === true ||
          +pressure_arr[0].trim() <= 20 ||
          +pressure_arr[0].trim() >=400 ||
          isNaN(pressure_arr[1].trim()) === true ||
          +pressure_arr[1].trim() <= 20 ||
          +pressure_arr[1].trim() >= 400 ||
          modal__pressure.value.indexOf("/") !== modal__pressure.value.lastIndexOf("/"))){
    // console.log('modal__pressure.value.indexOf("/"): ', modal__pressure.value.indexOf("/"));
    alert("Введіть тиск в форматі хх/хх, наприклад 120/80. Значення мають бути в діаназоні 20...400");
  }
  //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  else { 
    let modal = new Modal(
            // modal_array.length, //ID = длине массива. В начале длина = 1, соотв. ID = 1;
            1, //Все равно бэкэнд свой ID вернет..
            modal__name.value.trim(), // name
            modal__doctor.value.trim(), // doctor
            modal_goal.value.trim(), //goal
            modal__description.value.trim(), // description
            modal__priority.value.trim(), // priority
            modal__pressure.value.trim(), // pressure
            modal__weight.value.trim(), //weight
            modal__heart.value.trim(), // heart
            modal__age.value.trim(), // age
            modal__last_visit.value.trim(), // last_visit
            modal__status.value.trim() // status
        );

        // Отправляем модифицированный объект modal в базу данных сервера
    //==== let token = "885f873a-f332-4e4a-98f5-fd0c310126e2";
    let token = localStorage.getItem("token");
        async function update_modal(token, modal) {
            let request = await fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
                method: 'PUT',
                headers: {
                  'Content-Type': 'application/json',
                  'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify(modal)
              });
            
            let response = await request.json();
            // Заменяем редактируемый элемент массива backend_cards бъектом от сервера
            backend_cards.splice(card_index, 1, response);
            localStorage.setItem("backend_cards", JSON.stringify(backend_cards)); /// STORAGE
            // console.log("========================",response);
            // console.log("backend_cards:", backend_cards);
          
          
          // Находим элементы карты с классом id${id} и изменяем ее содержимое.
        //let card_to_edit = document.querySelector(`.id${id}`); //`.id${obj.id} .visit__more-info`
        let status = document.querySelector(`.id${id} .visit__status`);
        let doctor = document.querySelector(`.id${id} .visit__doctor`);
        let goal = document.querySelector(`.id${id} .visit__more-info .visit__purpose .purpose__value`);
        let description = document.querySelector(`.id${id} .visit__more-info .visit__brief .brief__value`);
        let priority = document.querySelector(`.id${id} .visit__more-info .visit__urgency .urgency__value`);
        let name = document.querySelector(`.id${id} .visit__fullname`);
        let pressure = document.querySelector(`.id${id} .visit__more-info .visit__pressure .pressure__value`);    
        let weight = document.querySelector(`.id${id} .visit__more-info .visit__mass-index .mass-index__value`);    
        let heart = document.querySelector(`.id${id} .visit__more-info .visit__disease .disease__value`);    
        let age = document.querySelector(`.id${id} .visit__more-info .visit__age .age__value`);    
        let last_visit = document.querySelector(`.id${id} .visit__more-info .visit__last-date-visit .last-date-visit__value`);
          
          // console.log(doctor, goal, description, priority, name, pressure, weight, heart, age, last_visit);
        
          if (response.doctor === "cardio") { 
            doctor.innerHTML = `<b>Кардіолог</b>`;
            document.querySelector(`.id${id} .visit__more-info .visit__purpose`).classList.remove("hidden");
            document.querySelector(`.id${id} .visit__more-info .visit__brief`).classList.remove("hidden");
            document.querySelector(`.id${id} .visit__more-info .visit__urgency`).classList.remove("hidden");
            document.querySelector(`.id${id} .visit__more-info .visit__pressure`).classList.remove("hidden");    
            document.querySelector(`.id${id} .visit__more-info .visit__mass-index`).classList.remove("hidden");    
            document.querySelector(`.id${id} .visit__more-info .visit__disease`).classList.remove("hidden");    
            document.querySelector(`.id${id} .visit__more-info .visit__age`).classList.remove("hidden");    
            document.querySelector(`.id${id} .visit__more-info .visit__last-date-visit`).classList.add("hidden");
          }
          if (response.doctor === "dentist") { 
            doctor.innerHTML = `<b>Стоматолог</b>`;
            document.querySelector(`.id${id} .visit__more-info .visit__purpose`).classList.remove("hidden");
            document.querySelector(`.id${id} .visit__more-info .visit__brief`).classList.remove("hidden");
            document.querySelector(`.id${id} .visit__more-info .visit__urgency`).classList.remove("hidden");
            document.querySelector(`.id${id} .visit__more-info .visit__pressure`).classList.add("hidden");    
            document.querySelector(`.id${id} .visit__more-info .visit__mass-index`).classList.add("hidden");    
            document.querySelector(`.id${id} .visit__more-info .visit__disease`).classList.add("hidden");    
            document.querySelector(`.id${id} .visit__more-info .visit__age`).classList.add("hidden");    
            document.querySelector(`.id${id} .visit__more-info .visit__last-date-visit`).classList.remove("hidden");
          }
          if (response.doctor === "thera") { 
            doctor.innerHTML = `<b>Терапевт</b>`;
            document.querySelector(`.id${id} .visit__more-info .visit__purpose`).classList.remove("hidden");
            document.querySelector(`.id${id} .visit__more-info .visit__brief`).classList.remove("hidden");
            document.querySelector(`.id${id} .visit__more-info .visit__urgency`).classList.remove("hidden");
            document.querySelector(`.id${id} .visit__more-info .visit__pressure`).classList.add("hidden");    
            document.querySelector(`.id${id} .visit__more-info .visit__mass-index`).classList.add("hidden");    
            document.querySelector(`.id${id} .visit__more-info .visit__disease`).classList.add("hidden");    
            document.querySelector(`.id${id} .visit__more-info .visit__age`).classList.remove("hidden");    
            document.querySelector(`.id${id} .visit__more-info .visit__last-date-visit`).classList.add("hidden");
          }
          
        goal.textContent = response.goal;
        description.textContent = response.description;
        priority.textContent = response.priority;
        name.textContent = response.name;
        pressure.textContent = response.pressure;
        weight.textContent = response.weight;
        heart.textContent = response.heart;
        age.textContent = response.age;
        last_visit.textContent = response.last_visit;
        status.textContent = response.status;
        
        }

        update_modal(token, modal);

        //После того, как отправили и сохранили все, что нужно, закрываем модальное
        // окно. При этом очищаем поля ввода.
        modal__cancel();
  }
    
 }
    ////===========================
}

show_current_modal();

}

export default edit_card;