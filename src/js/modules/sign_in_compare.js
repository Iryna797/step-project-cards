import show_cards from "./show_cards.js";

async function sign_in_compare() {
    let sign_in_btn = document.querySelector(".login-btn");
    let visit_btn = document.querySelector(".create-btn");
    let sign_in_login = document.querySelector(".sign_in_login");
    let sign_in_pass = document.querySelector(".sign_in_pass");
    //==== let user_login = localStorage.getItem("user_login");
    //==== let user_pass = localStorage.getItem("user_pass");
    let sign_in_form = document.querySelector(".sign_in_form");

    //=============================================
    // При входе вводим логин (почта) и пароль, которые использовались при регистрации,
    // когда получили токен:
    //          testcards@gmail.com
    //          436687
    //          Your token is: 885f873a-f332-4e4a-98f5-fd0c310126e2
    // Делаем ПОСТ запрос, отправляем туда регистрационные данные. Если данные ОК, сервер
    // вернет токен (такой же, как при регистрации). Его запоминаем в ЛокалСторэдж и
    // используем дальше в работе.

    let request = await fetch("https://ajax.test-danit.com/api/v2/cards/login", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            email: sign_in_login.value.trim(),
            password: sign_in_pass.value
        })
    });
    let response = await request.text();
    // console.log("token from server:", response);
    localStorage.setItem("token", response);

    //=============================================


    //==== if (sign_in_login.value === user_login && sign_in_pass.value === user_pass) { 
    if (response !== "Incorrect username or password") { 
        sign_in_form.classList.add("hidden");
        visit_btn.classList.remove("hidden");
        sign_in_btn.classList.add("hidden");

        // Если пользователь залогинился, нужно достать с сервера все ранее созданные 
        // карточки и отобразить их на странице
        //===== let token = "885f873a-f332-4e4a-98f5-fd0c310126e2";
        
        show_cards(response);
    }

}

export default sign_in_compare;