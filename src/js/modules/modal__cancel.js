function modal__cancel() {
    let modal = document.querySelector(".modal");
    let modal_goal = document.querySelector(".modal_goal");
    let modal__description = document.querySelector(".modal__description");
    let modal__name = document.querySelector(".modal__name");
    let modal__pressure = document.querySelector(".modal__pressure");
    let modal__weight = document.querySelector(".modal__weight");
    let modal__heart = document.querySelector(".modal__heart");
    let modal__age = document.querySelector(".modal__age");
    let modal__last_visit = document.querySelector(".modal__last_visit");

    modal.classList.add("hidden");
    modal_goal.value = "";
    modal__description.value = "";
    modal__name.value = "";
    modal__pressure.value = "";
    modal__weight.value = "";
    modal__heart.value = "";
    modal__age.value = "";
    modal__last_visit.value = "";
}

export default modal__cancel;