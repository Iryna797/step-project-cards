import modal__cancel from "./modal__cancel.js";
import Modal from "./Modal.js";
import VisitCardiologist from "./VisitCardiologist.js";
import VisitDentist from "./VisitDentist.js";
import VisitTherapist from "./VisitTherapist.js";
import del_card from "./del_card.js";
import edit_card from "./edit_card.js";

function modal_accept(modal_array) {

    // console.log("modal_array, backend_cards->", modal_array, backend_cards);

    let modal__status = document.querySelector(".modal__status");
    let modal__doctor = document.querySelector(".modal__doctor");
    let modal_goal = document.querySelector(".modal_goal");
    let modal__description = document.querySelector(".modal__description");
    let modal__name = document.querySelector(".modal__name");
    let modal__priority = document.querySelector(".modal__priority");
    let modal__pressure = document.querySelector(".modal__pressure");
    let modal__weight = document.querySelector(".modal__weight");
    let modal__heart = document.querySelector(".modal__heart");
    let modal__age = document.querySelector(".modal__age");
    let modal__last_visit = document.querySelector(".modal__last_visit");
        // console.log(modal__doctor.selectedIndex);
        // console.log(modal__doctor.value);
//================
// Проверка полей на "пустоту". Если пусто - показываем алерт "введи инфо!"
  let pressure_arr = modal__pressure.value.split("/");

  if (modal_goal.value === "" || modal__description.value === "" || modal__name.value === "") {
    alert("Будь-ласка, заповніть дані");
  }
  //=-=-=-=-=-=-=-==-=-=-=-=-==-=-=-=-=-=-=-
  else if(modal__doctor.value === "cardio" && (modal__pressure.value.indexOf("/") === -1 ||
          isNaN(pressure_arr[0].trim()) === true ||
          +pressure_arr[0].trim() <= 20 ||
          +pressure_arr[0].trim() >=400 ||
          isNaN(pressure_arr[1].trim()) === true ||
          +pressure_arr[1].trim() <= 20 ||
          +pressure_arr[1].trim() >= 400 ||
          modal__pressure.value.indexOf("/") !== modal__pressure.value.lastIndexOf("/"))){
    // console.log('modal__pressure.value.indexOf("/"): ', modal__pressure.value.indexOf("/"));
    alert("Введіть тиск в форматі хх/хх, наприклад 120/80. Значення мають бути в діаназоні 20...400");
  }
  //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  else { 
    if(localStorage.getItem("modal_array")) {
            modal_array = JSON.parse(localStorage.getItem("modal_array"));
        }

        // Записываем полную информацию из модального окна, без специфики по конкретному
        // доктору. Так все и будем отправлять в базу данных сервера.
        // Разделять по докторам будем при получении ответа от сервера. Тогда и будем 
        // создавать экземпляры классов Кардиолога, Стоматолога, Терапевта.

        let modal = new Modal(
            // modal_array.length, //ID = длине массива. В начале длина = 1, соотв. ID = 1;
            1, // Backend вернет свой ID, нет смысла давать..
            modal__name.value.trim(), // name
            modal__doctor.value.trim(), // doctor
            modal_goal.value.trim(), //goal
            modal__description.value.trim(), // description
            modal__priority.value.trim(), // priority
            modal__pressure.value.trim(), // pressure
            modal__weight.value.trim(), //weight
            modal__heart.value.trim(), // heart
            modal__age.value.trim(), // age
            modal__last_visit.value.trim(), // last_visit
            modal__status.value.trim() // status
        );

        modal_array.push(modal);
        // backend_cards.push(modal);

        localStorage.setItem("modal_array", JSON.stringify(modal_array));
        // console.log("modal_array:", modal_array);
        // console.log("backend_cards:", backend_cards);

        // Отправляем созданный объект modal в базу данных сервера
    //==== let token = "885f873a-f332-4e4a-98f5-fd0c310126e2";
    let token = localStorage.getItem("token");
        async function send_modal(token, modal) {
            let request = await fetch("https://ajax.test-danit.com/api/v2/cards", {
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json',
                  'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify(modal)
              });
            
            let response = await request.json();
            // console.log("backend_cards BEFORE:", backend_cards);
            let backend_cards = JSON.parse(localStorage.getItem("backend_cards")); ///STORAGE
            backend_cards.push(response);
            localStorage.setItem("backend_cards", JSON.stringify(backend_cards)); ///STORAGE
            // console.log(response);
            // console.log("backend_cards:", backend_cards);
          
          //Добавляем в блок visits_block созданную карту (рисуем на экране):
          if(response.doctor === "dentist"){
            let visit_dentist = new VisitDentist(
              response.id,
              response.name,
              response.goal,
              response.description,
              response.priority,
              response.last_visit,
              response.status
            );
                    
            visit_dentist.render(response.id);
            // Добавляем события на кнопки delete, more карточки
            let btn_delete = document.querySelector(`.id${response.id} .visit__btn-control .btn-delete`);
            btn_delete.addEventListener("click", () => { del_card(response.id) });
            let btn_more = document.querySelector(`.id${response.id} .visit__btn-control .btn-more`);
            btn_more.onclick = () => { 
              document.querySelector(`.id${response.id} .visit__more-info`).classList.toggle("hidden");
            }
            // Кнопка btn-edit
            let btn_edit = document.querySelector(`.id${response.id} .visit__btn-control .btn-edit`);
            btn_edit.addEventListener("click", () => { edit_card(response.id) });
          }
          if(response.doctor === "cardio"){
            let visit_cardio = new VisitCardiologist(
              response.id,
              response.name,
              response.goal,
              response.description,
              response.priority,
              response.pressure,
              response.weight,
              response.heart,
              response.age,
              response.status
            );
            visit_cardio.render(response.id);
            // Добавляем события на кнопки delete, more карточки
            let btn_delete = document.querySelector(`.id${response.id} .visit__btn-control .btn-delete`);
            btn_delete.addEventListener("click", () => { del_card(response.id) });
            let btn_more = document.querySelector(`.id${response.id} .visit__btn-control .btn-more`);
            btn_more.onclick = () => { 
              document.querySelector(`.id${response.id} .visit__more-info`).classList.toggle("hidden");
            }
            // Кнопка btn-edit
            let btn_edit = document.querySelector(`.id${response.id} .visit__btn-control .btn-edit`);
            btn_edit.addEventListener("click", () => { edit_card(response.id) });
          }
          if(response.doctor === "thera"){
            let visit_thera = new VisitTherapist(
              response.id,
              response.name,
              response.goal,
              response.description,
              response.priority,
              response.age,
              response.status
            );
            visit_thera.render(response.id);
            // Добавляем события на кнопки delete, more карточки
            let btn_delete = document.querySelector(`.id${response.id} .visit__btn-control .btn-delete`);
            btn_delete.addEventListener("click", () => { del_card(response.id) });
            let btn_more = document.querySelector(`.id${response.id} .visit__btn-control .btn-more`);
            btn_more.onclick = () => { 
              document.querySelector(`.id${response.id} .visit__more-info`).classList.toggle("hidden");
            }
            // Кнопка btn-edit
            let btn_edit = document.querySelector(`.id${response.id} .visit__btn-control .btn-edit`);
            btn_edit.addEventListener("click", () => { edit_card(response.id) });
          }
        }

        send_modal(token, modal);

        //После того, как отправили и сохранили все, что нужно, закрываем модальное
        // окно. При этом очищаем поля ввода.
        modal__cancel();
  }
//=================
    
}

export default modal_accept;