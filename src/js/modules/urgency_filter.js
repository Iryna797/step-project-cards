function urgency_filter(e) {
  // console.log(e.target.value);
  let all_urgency = document.querySelectorAll(".visit .visit__more-info .visit__urgency .urgency__value");

  if (e.target.value === "low") { 
    for (let elem of all_urgency) {    
      if (elem.textContent === "Звичайна") { 
        elem.parentElement.parentElement.parentElement.classList.remove("hidden");
      }
      if (elem.textContent === "Пріоритетна") { 
        elem.parentElement.parentElement.parentElement.classList.add("hidden");
      }
      if (elem.textContent === "Невідкладна") { 
        elem.parentElement.parentElement.parentElement.classList.add("hidden");
      }
    }
  }
  if (e.target.value === "normal") { 
    for (let elem of all_urgency) {    
      if (elem.textContent === "Звичайна") { 
        elem.parentElement.parentElement.parentElement.classList.add("hidden");
      }
      if (elem.textContent === "Пріоритетна") { 
        elem.parentElement.parentElement.parentElement.classList.remove("hidden");
      }
      if (elem.textContent === "Невідкладна") { 
        elem.parentElement.parentElement.parentElement.classList.add("hidden");
      }
    }
  }
  if (e.target.value === "high") { 
    for (let elem of all_urgency) {    
      if (elem.textContent === "Звичайна") { 
        elem.parentElement.parentElement.parentElement.classList.add("hidden");
      }
      if (elem.textContent === "Пріоритетна") { 
        elem.parentElement.parentElement.parentElement.classList.add("hidden");
      }
      if (elem.textContent === "Невідкладна") { 
        elem.parentElement.parentElement.parentElement.classList.remove("hidden");
      }
    }
  }
  if (e.target.value === "null") { 
    for (let elem of all_urgency) {    
      if (elem.textContent === "Звичайна") { 
        elem.parentElement.parentElement.parentElement.classList.remove("hidden");
      }
      if (elem.textContent === "Пріоритетна") { 
        elem.parentElement.parentElement.parentElement.classList.remove("hidden");
      }
      if (elem.textContent === "Невідкладна") { 
        elem.parentElement.parentElement.parentElement.classList.remove("hidden");
      }
    }
  } 
}

export default urgency_filter;