function status_filter(e) {
  // console.log(e.target.value);
  let all_status = document.querySelectorAll(".visit .visit__status");

  if (e.target.value === "open") { 
    for (let elem of all_status) {    
      if (elem.textContent === "Заплановано") { 
        elem.parentElement.classList.remove("hidden");
      }
      if (elem.textContent === "Виконано") { 
        elem.parentElement.classList.add("hidden");
      }
    }
  }
  if (e.target.value === "done") { 
    for (let elem of all_status) {    
      if (elem.textContent === "Заплановано") { 
        elem.parentElement.classList.add("hidden");
      }
      if (elem.textContent === "Виконано") { 
        elem.parentElement.classList.remove("hidden");
      }
    }
  }
  if (e.target.value === "null") { 
    for (let elem of all_status) {    
      if (elem.textContent === "Заплановано") { 
        elem.parentElement.classList.remove("hidden");
      }
      if (elem.textContent === "Виконано") { 
        elem.parentElement.classList.remove("hidden");
      }
    }
  }
  
}

export default status_filter;